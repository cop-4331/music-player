import asyncHandler from 'express-async-handler';
import UserModel from '../models/users.js';

// @desc    Gets a user
// @route   GET /users
// @access  Private
const getUsers = asyncHandler(async (req, res) => {
  const users = await UserModel.find();
  res.status(200).json(users);
});

// @desc    Sets a user
// @route   POST /users
// @access  Private
const createUser = asyncHandler(async (req, res) => {
  console.log(req.body);

  const user = await UserModel.create({
    Username: req.body.Username,
    Password: req.body.Password,
  });

  res.status(200).json(user);
});

// @desc    Updates a user
// @route   PUT /users/:id
// @access  Private
const updateUser = asyncHandler(async (req, res) => {
  const user = UserModel.findById(req.params.id);

  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  const updatedUser = await UserModel.findByIdAndUpdate(req.params.id, req.body);

  res.status(200).json(updatedUser);
});

// @desc    Deletes a user
// @route   DELETE /users/:id
// @access  Private
const deleteUser = asyncHandler(async (req, res) => {
  const user = UserModel.findById(req.params.id);

  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  await user.remove();

  res.status(200).json({ id: req.params.id });
});

export { getUsers, createUser, updateUser, deleteUser };

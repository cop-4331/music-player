import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  Username: {
    type: String,
    required: true,
  },
  Password: {
    type: String,
    required: true,
  },
  PlaylistIDs: {
    type: [mongoose.SchemaTypes.ObjectId],
    required: false,
  },
  LastPlaylistID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: false,
  },
  LastSongID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: false,
  },
});

const UserModel = mongoose.model('users', UserSchema);
export default UserModel;

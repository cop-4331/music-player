import mongoose from 'mongoose';

const PlaylistSchema = new mongoose.Schema({
  UserID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  PlaylistName: {
    type: String,
    required: true,
  },
  Songs: {
    type: [mongoose.SchemaType.ObjectId],
    required: true,
  },
});

const PlaylistModel = mongoose.model('playlists', PlaylistSchema);
export default PlaylistModel;

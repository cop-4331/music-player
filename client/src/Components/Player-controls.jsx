/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import {
  faPlay,
  faPause,
  faForward,
  faBackward,
  faRepeat,
  faHeart,
} from '@fortawesome/free-solid-svg-icons';
import './css/Music-screen.css';

function PlayerControls(props) {
  return (
    <div className="c-player--controls">
      <button className="loop-btn" onClick={() => props.loopSong(!props.cnt)}>
        <FontAwesomeIcon icon={props.cnt ? faHeart : faRepeat} />
      </button>
      <button className="skip-btn" onClick={() => props.SkipSong(false)}>
        <FontAwesomeIcon icon={faBackward} />
      </button>
      <button className="play-btn" onClick={() => props.setIsPlaying(!props.isPlaying)}>
        <FontAwesomeIcon icon={props.isPlaying ? faPause : faPlay} />
      </button>
      <button className="skip-btn" onClick={() => props.SkipSong()}>
        <FontAwesomeIcon icon={faForward} />
      </button>
      {/* <button className="shuffle-btn-on" onClick={() => props.shuffleSongs(false)}>
        <FontAwesomeIcon icon={faShuffle} />
      </button> */}
    </div>
  );
}

export default PlayerControls;

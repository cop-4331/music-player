/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-unused-expressions */
/* eslint-disable import/no-duplicates */
/* eslint-disable react/jsx-no-undef */
import 'react-pro-sidebar/dist/css/styles.css';

// https://codesandbox.io/s/dpdb1?file=/src/Components/Header/Header.js:641-663
// https://flowbite.com/docs/components/buttons/

// import useState hook to create menu collapse state
// eslint-disable-next-line no-unused-vars
import React, { useState } from 'react';
import './css/Sidebar-right.css';

// import react pro sidebar components
import {
  ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from 'react-pro-sidebar';

// import icons from react icons
import { FiArrowLeftCircle, FiArrowRightCircle, FiEdit, FiSearch } from 'react-icons/fi';
import { GiMusicalNotes } from 'react-icons/gi';

// import sidebar css from react-pro-sidebar module and our custom css
import './css/Sidebar-right.css';

// eslint-disable-next-line no-unused-vars
function SidebarRight() {
  const [menuCollapse, setMenuCollapse] = useState(false);

  // create a custom function that will change menucollapse state from false to true and true to false
  const menuIconClick = () => {
    // condition checking to change state from true to false and vice versa
    menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
  };

  const playlist = [
    <>
      <MenuItem> Waka Waka </MenuItem>
      <MenuItem> Dancin </MenuItem>
      <MenuItem> I Am So T I R E D </MenuItem>
      <MenuItem> Punga </MenuItem>
    </>,
  ];

  return (
    <div id="sidebar-right">
      {/* collapsed props to change menu size using menucollapse state */}
      <ProSidebar collapsed={menuCollapse}>
        <SidebarHeader>
          <div className="logotext">
            {/* small and big change using menucollapse state */}
            <p>{menuCollapse ? <GiMusicalNotes /> : 'Playing Now'}</p>
          </div>
          <div className="closemenu" onClick={menuIconClick}>
            {/* changing menu collapse icon on click */}
            {menuCollapse ? <FiArrowLeftCircle /> : <FiArrowRightCircle />}
          </div>
        </SidebarHeader>
        {/* // _________________ */}
        <SidebarContent>
          <Menu iconShape="square">
            <MenuItem active icon={<GiMusicalNotes />}>
              Selected Playlist
            </MenuItem>
            <MenuItem icon={<GiMusicalNotes />}>{playlist}</MenuItem>
            {/* <SubMenu title="My Playlists" icon={<GiMusicalNotes />}>
              <SubMenu title="Playlist A">{playlist}</SubMenu>
            </SubMenu> */}
          </Menu>
        </SidebarContent>
        {/* // _________________ */}
        <SidebarFooter>
          <Menu iconShape="square">
            <MenuItem icon={<FiSearch />}> Search Playlist</MenuItem>
          </Menu>
        </SidebarFooter>
        <SidebarFooter>
          <Menu iconShape="square">
            <MenuItem icon={<FiEdit />}> Edit Playlist</MenuItem>
          </Menu>
        </SidebarFooter>
      </ProSidebar>
    </div>
  );
}

export default SidebarRight;

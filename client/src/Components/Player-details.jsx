/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-console */
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import './css/Music-screen.css';

function PlayerDetails(props) {
  return (
    <div className="c-player--details">
      <div className="details-img">
        <img src={require(`./images/${props.song.img_src}.jpg`)} alt="" />
      </div>
      <h3 className="details-title">{props.song.title}</h3>
      <h4 className="details-artist">{props.song.artist}</h4>
    </div>
  );
}
export default PlayerDetails;

/* eslint-disable no-param-reassign */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
/* eslint-disable jsx-a11y/media-has-caption */
/* eslint-disable no-plusplus */
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-filename-extension */
import React, { useState, useRef, useEffect } from 'react';
import PlayerDetails from './Player-details';
import PlayerControls from './Player-controls';
import './css/Music-screen.css';

function Player(props) {
  const songAudio = useRef(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [cnt, loopSong] = useState(false);
  // const [shuffle, shuffleSong] = useState(false);

  const player = document.getElementById('audio-control');
  const [duration, setDuration] = useState(0);

  useEffect(() => {
    if (isPlaying) {
      songAudio.current.play();
    } else {
      songAudio.current.pause();
    }
  });

  useEffect(() => {
    if (cnt) {
      songAudio.current.loop = true;
    } else {
      songAudio.current.loop = false;
    }
  }, [cnt]);

  const SkipSong = (forwards = true) => {
    if (forwards) {
      props.setCurrentSongIndex(() => {
        let temp = props.currentSongIndex;
        temp++;

        if (temp > props.songs.length - 1) {
          temp = 0;
        }

        return temp;
      });
    } else {
      props.setCurrentSongIndex(() => {
        let temp = props.currentSongIndex;
        temp--;

        if (temp < 0) {
          temp = props.songs.length - 1;
        }

        return temp;
      });
    }
  };

  // const shuffleSong = (shuffleS = false) => {
  //   const copy = [...props.songs];
  //   if (shuffleS) {
  //     props.copy.sort(() => 0.5 - Math.random());

  //     props.setCurrentSongIndex(() => {
  //       let temp = props.currentSongIndex;

  //       if (temp > props.copy.length - 1) {
  //         temp = 0;
  //       }
  //       return temp;
  //     });
  //   } else {
  //     props.setCurrentSongIndex(() => {
  //       let temp = props.currentSongIndex;
  //       if (temp < 0) {
  //         temp = props.copy.length - 1;
  //       }
  //       return temp;
  //     });
  //   }
  // };

  return (
    <div className="c-player">
      <audio
        id="audio-control"
        src={require(`./music/${props.songs[props.currentSongIndex].src}.mp3`)}
        ref={songAudio}
        // loop
        controls
        // onEnded={() => props.SkipSong(true)}
      />
      <h4>Playing now</h4>
      <PlayerDetails song={props.songs[props.currentSongIndex]} />
      <PlayerControls
        isPlaying={isPlaying}
        setIsPlaying={setIsPlaying}
        SkipSong={SkipSong}
        loopSong={loopSong}
        cnt={cnt}
        // shuffleSong={shuffleSong}
      />
      <p>
        Next up:{' '}
        <span>
          {props.songs[props.nextSongIndex].title} by {props.songs[props.nextSongIndex].artist}
        </span>
      </p>
    </div>
  );
}

export default Player;

/* eslint-disable import/no-duplicates */
/* eslint-disable consistent-return */
import { useEffect } from 'react';
import { useState } from 'react';
import Player from './Player';
import './css/Music-screen.css';

function MusicScreen() {
  const [songs] = useState([
    {
      title: 'Billie Jean',
      artist: 'Michael Jackson',
      img_src: 'billiejean',
      src: 'BillieJean',
    },
    {
      title: 'Purple Rain',
      artist: 'Prince',
      img_src: 'purple-rain',
      src: 'PurpleRain',
    },
    {
      title: 'Umbrella',
      artist: 'Rihanna',
      img_src: 'umbrella',
      src: 'Umbrella',
    },
    {
      title: 'Tic Tic Tac',
      artist: 'Carrapicho',
      img_src: 'tic-tic-tac',
      src: 'TicTicTac',
    },
    {
      title: 'test',
      artist: 'teiuhk',
      img_src: 'billiejean',
      src: 'three',
    },
  ]);

  const [currentSongIndex, setCurrentSongIndex] = useState(0);
  const [nextSongIndex, setNextSongIndex] = useState(currentSongIndex + 1);
  useEffect(() => {
    setNextSongIndex(() => {
      if (currentSongIndex + 1 > songs.length - 1) {
        return 0;
      }
      return currentSongIndex + 1;
    });
  }, [currentSongIndex]);

  return (
    <div className="MusicScreen">
      <Player
        currentSongIndex={currentSongIndex}
        setCurrentSongIndex={setCurrentSongIndex}
        nextSongIndex={nextSongIndex}
        songs={songs}
      />
    </div>
  );
}

export default MusicScreen;

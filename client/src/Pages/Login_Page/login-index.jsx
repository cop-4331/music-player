/* eslint-disable react/jsx-no-bind */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Fragment, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';

function MyModal() {
  const [isOpen, setIsOpen] = useState(false); // Initially, do not have the pop-up on

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  return (
    <>
      <div className="px-4">
        <button
          type="button"
          onClick={openModal}
          className="px-4 py-2 text-sm font-medium text-white bg-black rounded-md hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
        >
          Sign up
        </button>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="fixed inset-0 z-10 overflow-y-auto" onClose={closeModal}>
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span className="inline-block h-screen align-middle" aria-hidden="true">
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900 px-2">
                  Registration
                </Dialog.Title>
                <div className="mt-2">
                  <br />
                  <label className="text-sm text-gray-700 px-2"> Username </label>
                  <input
                    className="text-sm px-1"
                    type="text"
                    name="thisIsWhatYouWouldGrabInTheServerSide(variable)"
                    placeholder="Enter username"
                  />

                  <br />
                  <br />

                  <label className="text-sm text-gray-700 px-2"> Password </label>
                  <input
                    className="text-sm px-1"
                    type="text"
                    name="password"
                    placeholder="Enter password"
                  />

                  <br />
                  <br />

                  <label className="text-sm text-gray-700 px-2"> Confirm Password </label>
                  <input
                    className="text-sm px-1"
                    type="text"
                    name="confirmPassword"
                    placeholder="Enter password again"
                  />
                </div>

                <div className="mt-4 px-1">
                  <button
                    type="button"
                    className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                    // onClick={closeModal} --> THIS NEEDS TO BE CHANGED WITH CHECKING IF THE PASSWORDS MATCH AND VALID USERNAME
                  >
                    Sign up
                  </button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}

// When returning, make sure you have things sandwiched with one component (like one <div> tag). If you have two different
// <div> tags, it will not work. If you want to do that, make sure that the two <div> tags are within one <div> tag.
// Also, you do not need to include all the html tags here. Just include whatever is in the body.
function Login() {
  return (
    <div>
      <h1 className="text-center py-4 text-lg font-bold leading-6 text-gray-900">
        Welcome to *insert music web app name*
      </h1>
      <form action="submitTheFormToThisPage.php" method="POST" className="py-4">
        <label className="px-4">Username</label>
        <input
          type="text"
          name="thisIsWhatYouWouldGrabInTheServerSide(variable)"
          placeholder="Enter username"
          className="px-1"
        />

        <br />
        <br />

        <label className="px-4">Password</label>
        <input type="text" name="password" placeholder="Enter password" className="px-1" />

        <br />
        <br />

        <div className="px-4">
          <button
            type="button"
            className="px-4 py-2 text-sm font-medium text-white bg-black rounded-md hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
          >
            Login
          </button>
        </div>

        <br />
        <MyModal />
      </form>
    </div>
  );
}

export default Login;

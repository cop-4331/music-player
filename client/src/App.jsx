// import React from 'react';
// import SimpleList from './Components/Sidebar-responsive';
import Sidebar from './Components/Sidebar-left';
import MusicScreen from './Components/Music-screen';
import SidebarRight from './Components/Sidebar-right';
import React from 'react';
import Login from './Pages/Login_Page/login-index';
import Example from './Pages/Login_Page/trial';
import './index.css';
// import Home from './Pages/Home Page/home';
// import Login from './Pages/Login Page/login-index';

function App() {
  return (
    // <ProSidebar>
    //   <Menu iconShape="square">
    //     <MenuItem>Dashboard</MenuItem>
    //     <SubMenu title="Components">
    //       <MenuItem>Component 1</MenuItem>
    //       <MenuItem>Component 2</MenuItem>
    //     </SubMenu>
    //   </Menu>
    // </ProSidebar>
    <div style={{ display: 'inline-flex' }}>
      <Sidebar />
      <MusicScreen />
      <SidebarRight />
      {/* <Home /> */}
    </div>
  );
}

export default App;
